VERSION 5.00
Object = "{EC3EDD2A-FDD8-4C80-B7DE-F6BD953082ED}#2.0#0"; "TimerRD.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3015
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   2160
      TabIndex        =   1
      Top             =   840
      Width           =   1215
   End
   Begin TimeRd.MyTimer MyTimer1 
      Left            =   960
      Top             =   840
      _ExtentX        =   714
      _ExtentY        =   714
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   375
      Left            =   1440
      TabIndex        =   0
      Top             =   1920
      Width           =   1695
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private T As Date

Private Sub Command1_Click()
If MyTimer1.Interval = 0 Then
T = Now
MyTimer1.Interval = 1
Else
MyTimer1.Interval = 0
End If
End Sub

Private Sub MyTimer1_Tick()
Label1.Caption = Format(Now - T, "HH:mm:ss") & "," & MyTimer1.Time
End Sub
