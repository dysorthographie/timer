VERSION 5.00
Begin VB.UserControl Horloge 
   BackStyle       =   0  'Transparent
   ClientHeight    =   3480
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3600
   ClipBehavior    =   0  'None
   ClipControls    =   0   'False
   DrawMode        =   8  'Xor Pen
   FillStyle       =   0  'Solid
   HitBehavior     =   0  'None
   MaskColor       =   &H80000001&
   PaletteMode     =   0  'Halftone
   ScaleHeight     =   61.383
   ScaleMode       =   0  'User
   ScaleWidth      =   63.5
   Begin VB.Timer Timer1 
      Left            =   360
      Top             =   3000
   End
   Begin VB.Image Image2 
      Height          =   3615
      Left            =   0
      Picture         =   "Horloge.ctx":0000
      Stretch         =   -1  'True
      Top             =   0
      Width           =   3615
   End
   Begin VB.Line Secondes 
      BorderColor     =   &H00FF0000&
      X1              =   10.583
      X2              =   35.983
      Y1              =   21.167
      Y2              =   29.633
   End
   Begin VB.Line Minutes 
      X1              =   33.867
      X2              =   33.867
      Y1              =   10.583
      Y2              =   25.4
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Lundi 30"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   960
      TabIndex        =   1
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Line Heures 
      BorderColor     =   &H00000000&
      X1              =   21.167
      X2              =   31.75
      Y1              =   10.583
      Y2              =   23.283
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00000000&
      Caption         =   "12:45:22"
      ForeColor       =   &H0000FF00&
      Height          =   375
      Left            =   1080
      TabIndex        =   0
      Top             =   2160
      Width           =   1455
   End
   Begin VB.Image Image1 
      Height          =   3615
      Left            =   0
      OLEDragMode     =   1  'Automatic
      OLEDropMode     =   2  'Automatic
      Stretch         =   -1  'True
      Top             =   0
      Width           =   3615
   End
End
Attribute VB_Name = "Horloge"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim Alpha As Double 'déplacement angulaire
Dim Ang As Double 'position angulaire
Dim iSec As Integer 'compteur de déplacement
Const PI = 3.14159265358979
Private DEP   As Double 'longueur de l'aiguille des secondes
Type Point
    x As Double
    y As Double
End Type
Public Property Get Interval() As Long
Interval = Timer1.Interval
End Property
Public Property Let Interval(value As Long)
Timer1.Interval = value
End Property
Public Property Get Quadrant() As Picture
Set Quadrant = Image1.Picture
End Property
Public Property Set Quadrant(value As Picture)
Image1.Picture = value
End Property
Private Sub Form_Load()

DEP = Mimage1.Height / 2
Heures.X1 = Image1.Left + (Image1.Width / 2)
Heures.X2 = Image1.Left + (Image1.Width / 2)
Heures.Y1 = Image1.Top + DEP
Heures.Y2 = Image1.Top
End Sub

Private Sub Timer1_Timer()
With XYSeconde(Secondes, Second(Time), DEP)
    Secondes.X2 = .x
    Secondes.Y2 = .y
End With
'With XYMinutes(Minutes, Minute(Time), DEP)
'    Minutes.X2 = .X
'    Minutes.Y2 = .Y
'End With
'With XYHeures(Heures, CInt(Format(Time, "h AMPM")), DEP)
'    Heures.X2 = .X
'    Heures.Y2 = .Y
'End With
Label1.Caption = Time
Label2.Caption = Format(Now, "DDDD DD")
DoEvents
End Sub
Private Sub UserControl_Resize()
Width = Height
DEP = Image1.Height / 2
Image1.Height = Height / 57
Image1.Width = Width / 57
DEP = Image1.Height / 2
Heures.X1 = Image1.Left + (Image1.Width / 2)
Heures.X2 = Image1.Left + (Image1.Width / 2)
Heures.Y1 = Image1.Top + DEP
Heures.Y2 = Image1.Top
Heures.BorderWidth = Image1.Height * 0.15

Minutes.X1 = Image1.Left + (Image1.Width / 2)
Minutes.X2 = Image1.Left + (Image1.Width / 2)
Minutes.Y1 = Image1.Top + DEP
Minutes.Y2 = Image1.Top
Minutes.BorderWidth = Image1.Height * 0.11

Secondes.X1 = Image1.Left + (Image1.Width / 2)
Secondes.X2 = Image1.Left + (Image1.Width / 2)
Secondes.Y1 = Image1.Top + DEP
Secondes.Y2 = Image1.Top
Secondes.BorderWidth = Image1.Height * 0.04

Label1.Height = Image1.Height * 0.1
Label1.Width = Image1.Width * 0.4
Label1.Top = Image1.Top + Image1.Height - (Label1.Height * 4.3)
Label1.Left = Image1.Left + (Image1.Width / 2) - (Label1.Width / 2.2)
Label1.FontSize = Label1.Height * 1.6


Label2.Height = Image1.Height * 0.1
Label2.Width = Image1.Width * 0.4
Label2.Top = Image1.Top + Image1.Height - (Label1.Height * 6.6)
Label2.Left = Image1.Left + (Image1.Width / 2) - (Label1.Width / 2.2)
Label2.FontSize = Label1.Height * 1.6




End Sub
Private Function XYSeconde(ByRef Ln As Line, ByVal Secondes As Integer, L As Double) As Point
Secondes = Secondes - 15
XYSeconde.x = Ln.X1 + Math.Cos(Secondes * (PI / 30)) * L
XYSeconde.y = Ln.Y1 + Math.Sin(Secondes * (PI / 30)) * L
End Function
Private Function XYMinutes(ByRef Ln As Line, ByVal Minutes As Integer, L As Double) As Point
Minutes = Minutes - 15
XYMinutes.x = Ln.X1 + Math.Cos(Minutes * (PI / 30)) * (L - (L * 0.2))
XYMinutes.y = Ln.Y1 + Math.Sin(Minutes * (PI / 30)) * (L - (L * 0.2))
End Function
Private Function XYHeures(ByRef Ln As Line, ByVal Heures As Integer, L As Double) As Point
Heures = Heures - 3
XYHeures.x = Ln.X1 + Math.Cos(Heures * (PI / 6)) * (L - (L * 0.5))
XYHeures.y = Ln.Y1 + Math.Sin(Heures * (PI / 6)) * (L - (L * 0.4))
End Function

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
With PropBag
.WriteProperty "Interval", Timer1.Interval, 0
End With
With PropBag
.WriteProperty "Quadrant", Image1.Picture, Image1.Picture
End With
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
With PropBag
Timer1.Interval = .ReadProperty("Interval", 0)
End With

With PropBag
Set Image1.Picture = .ReadProperty("Quadrant", Image1.Picture)
End With
End Sub
