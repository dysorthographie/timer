VERSION 5.00
Begin VB.UserControl MyTimer 
   ClientHeight    =   405
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   405
   InvisibleAtRuntime=   -1  'True
   ScaleHeight     =   405
   ScaleWidth      =   405
   ToolboxBitmap   =   "MyTimer.ctx":0000
   Begin VB.Timer Timer1 
      Left            =   0
      Top             =   0
   End
   Begin VB.Image Image1 
      Height          =   400
      Left            =   0
      Picture         =   "MyTimer.ctx":0312
      Stretch         =   -1  'True
      Top             =   0
      Width           =   400
   End
End
Attribute VB_Name = "MyTimer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Event Tick()
Public Property Get Interval() As Long
Interval = Timer1.Interval
End Property


Public Property Let Interval(value As Long)
Timer1.Interval = value
End Property
Public Property Get Time() As Long
Time = Right(Format(Timer, "0.0000"), 4)
End Property


Private Sub Timer1_Timer()
RaiseEvent Tick
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
With PropBag
Timer1.Interval = .ReadProperty("Interval", 0)
End With
End Sub

Private Sub UserControl_Resize()
Width = 400
Height = 400
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
With PropBag
.WriteProperty "Interval", Timer1.Interval, 0
End With
End Sub

